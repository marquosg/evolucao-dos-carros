# Evolução dos carros

Projeto realizado para meu trabalho de conclusão de curso do ensino médio em 2018. Utiliza de mecanismos de evolução e algoritmos genéticos para funcionar e evoluir, além da biblioteca p5 para rodar. O código aqui é inspirado em um video semelhante do canal CodeBullet no youtube.

Este foi meu primeiro projeto do tipo, então com certeza há muito espaço para melhorar, mas queria deixar registrado.
