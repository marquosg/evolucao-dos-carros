var popu;
var vida = 200;
var vidaP;
var fitnessP;
var contar = 0;
var objetivo;
var formax = 0.8;
var geracoes=1;
var geracaoP;
function setup() {
      createCanvas(400, 300);
      vidaP = createP();
      fitnessP = createP();
      geracaoP = createP();
      popu = new Popu();
      objetivo = createVector(5, height-240);
}

function draw() {
      background(0);
      popu.run();
      vidaP.html(contar);
      geracaoP.html(geracoes);
      contar++;
      
      if(contar == vida){
          popu.avaliar();
          popu.selecao();
          contar = 0;
          geracoes++;
      }
      fill(255);
      rect(0, height-105, width-140, 10);//inferior mais alto
      rect(0, height-15, width-40, 10);//inferior mais baixo
      rect(width-150, height-195, 10, 100);//em pé de dentro
      rect(width-40, height-285, 10, 280); //empé de fora
      rect(0, height-195, width-140, 10);//superior central 
      rect(0, height-285, width-40, 10);//superior exterior 80
      
      ellipse(objetivo.x, objetivo.y, 16, 16);
}


function DNA(genes){
  if(genes){
      this.genes = genes;
  } else {
      this.genes = [];
      for (var i=0; i < vida; i++){
        this.genes[i] = p5.Vector.random2D();
        this.genes[i].setMag(formax);
      }
  }
  this.crossover = function(parceiro){
    var ngenes = [];
    var meio = floor(random(this.genes.length));
    for (var i=0; i < this.genes.length; i++){
      if(i > meio){
        ngenes[i] = this.genes[i];
      } else {
        ngenes[i] = parceiro.genes[i];
      }
    }
    return new DNA(ngenes);
  }
  
  this.mutacao = function(){
    for (var i=0; i < this.genes.length; i++){
      if(random(1)< 0.025){
          this.genes[i] = p5.Vector.random2D();
          this.genes[i].setMag(formax);
        }
    }
  
  }
}


function Popu(){
  this.carros = [];
  this.poptam = 150;
  this.cruzamento = [];
  
  for(var i=0; i < this.poptam; i++){
    this.carros[i] = new Carro();
  }
  
this.avaliar = function(){
       var maxfit = 0;
       for(var i=0; i < this.poptam; i++){
        this.carros[i].calcFitness();
      
        if (this.carros[i].fitness > maxfit){
          
          maxfit = this.carros[i].fitness;
        }
    }
    fitnessP.html(maxfit);
    
    for(var i=0; i < this.poptam; i++){
      this.carros[i].fitness /= maxfit;
    }
    
    this.cruzamento = [];
    
    for(var i=0; i < this.poptam; i++){
      var n = this.carros[i].fitness*500;
      for(var u=0; u < n; u++){
        this.cruzamento.push(this.carros[i]);
      }
    }
    
    
  }
  
  this.selecao = function(){
    var novosCarros = [];
       for(var i=0; i < this.carros.length; i++){
          var pai = random(this.cruzamento).dna;
          var mae = random(this.cruzamento).dna;
          var filho = pai.crossover(mae);
          filho.mutacao();
          novosCarros[i] = new Carro(filho);
       }
       this.carros = novosCarros;
  }
  
      this.run = function(){
          for(var i=0; i < this.poptam; i++){
          this.carros[i].update();
          this.carros[i].show();
        }
  }
}



function Carro(dna){

  this.pos = createVector(5, height-60); 
  this.vel = createVector();
  this.ace = createVector();
  this.cabo = false;
  this.bateu = false;
  if(dna){
    this.dna = dna;
  } else {
    this.dna = new DNA();
  }
  
  this.fitness = 0;
  
  this.aplicarForca = function(forca){
    this.ace.add(forca);
  }
  
        this.update = function(){
          var d = dist(this.pos.x, this.pos.y, objetivo.x, objetivo.y);
          if ( d < 10){
              this.cabo = true;
              this.pos = objetivo.copy();
          }
          /*rect(0, height-105, width-150, 10);//inferior mais alto
      rect(0, height-15, width-50, 10);//inferior mais baixo
      rect(width-150, height-195, 10, 100);//em pé de dentro
      rect(width-40, height-285, 10, 280); //empé de fora
      rect(0, height-195, width-150, 10);//superior central 
      rect(0, height-285, width-50, 10);//superior exterior 80*/
          if(this.pos.x < 0){
              this.bateu = true;
          }
          if(this.pos.x > width-40){
              this.bateu = true;
          }
          
          if(this.pos.y > height-15){
              this.bateu = true;
          }
          if(this.pos.y < height-275){
              this.bateu = true;
          }
          if(this.pos.x < width-140 &&  this.pos.y < height- 95 && this.pos.y > height-195 ){
              this.bateu = true;
          }
          
              this.aplicarForca(this.dna.genes[contar]);
          if(!this.completed && !this.bateu){
              this.vel.add(this.ace);
              this.pos.add(this.vel);
              this.ace.mult(0);
              this.vel.limit(6);
          }
  }
  
  this.calcFitness = function(){
    var d = dist(this.pos.x, this.pos.y, objetivo.x, objetivo.y);
    var s = dist(this.pos.x, this.pos.y, 5, 240);
    var dfitness = Math.abs(map(d, 0, width, width, 0));// 1/d
    var sfitness = Math.abs(map(s, 0, width, width, 0));// 1/s
    this.fitness = 1000*(s/d);
    
    if(this.cabo){
      this.fitness *= 100;
    }
    if(this.bateu){
      this.fitness /= 50 ;
    }
    
  }
  
  
  this.show = function(){
    push();
    noStroke();
    fill(255, 150);
    translate(this.pos.x, this.pos.y);
    rotate(this.vel.heading());
    rectMode(CENTER);
    rect(0, 0, 25, 5);
    pop();
  }
}
